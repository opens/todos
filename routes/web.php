<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/index', function () {
    return view('todos.index');
});
Route::get('/create', function () {
    $users = User::all()->pluck('name', 'id');
    return view('todos.create', compact('users'));
});
Route::get('/edit/{id}', function ($id) {
    $users = User::all()->pluck('name', 'id');
    return view('todos.edit', compact('id', 'users'));
});
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
