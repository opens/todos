<?php

namespace Database\Seeders;

use App\Models\TodoList;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()
            ->count(10)
            ->create()
            ->each(function ($user) {
                TodoList::factory()
                    ->count(20)
                    ->create([
                        'user_id' => $user->id,
                        'date' => Carbon::now()->startOfMonth()->addDays(rand(0, 30)),
                    ]);

                TodoList::factory()
                    ->count(10)
                    ->create([
                        'user_id' => $user->id,
                        'date' => Carbon::now()->subWeek()->addDays(rand(0, 6)),
                    ]);

                TodoList::factory()
                    ->count(5)
                    ->create([
                        'user_id' => $user->id,
                        'date' => Carbon::now(),
                    ]);
            });
    }
}
