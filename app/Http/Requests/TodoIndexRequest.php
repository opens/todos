<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TodoIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'filter' => 'array',
            'filter.*' => 'array',
            'filter.*.contains' => 'string',
            'filter.*.from' => 'date',
            'filter.*.to' => 'date',
            'sort' => 'array',
            'sort.*.field' => 'required|string',
            'sort.*.value' => 'required|in:asc,desc',
            'startRow' => 'integer|min:0',
            'endRow' => 'integer|min:0|gte:startRow',
        ];
    }
}
