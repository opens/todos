<?php

namespace App\Http\Controllers;

use App\Models\TodoList;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $todayTasks = TodoList::whereDate('date', Carbon::today())->count();
        $weekTasks = TodoList::whereBetween('date', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $monthTasks = TodoList::whereMonth('date', Carbon::now()->month)->count();
        $tasksData = TodoList::select('date', DB::raw('count(*) as task_count'))
            ->groupBy('date')
            ->get();

        return view('dashboard', compact('tasksData', 'todayTasks', 'weekTasks', 'monthTasks'));
    }
}
