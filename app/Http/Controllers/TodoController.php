<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoIndexRequest;
use App\Http\Requests\TodoRequest;
use App\Http\Resources\TodoResource;
use App\Models\TodoList;

class TodoController extends Controller
{
    public function index(TodoIndexRequest $request)
    {
        $filters = $request->input('filter', []);
        $sort = $request->input('sort', []);
        $startRow = $request->input('startRow', 0);
        $endRow = $request->input('endRow', 10);
        $query = TodoList::with('user');

        $filterableFields = [
            'completed' => 'boolean',
            'description' => 'text',
            'user' => 'text',
            'title' => 'text',
            'date' => 'date',
            'created_at' => 'date',
            'updated_at' => 'date',
        ];

        foreach ($filters as $field => $value) {
            if (array_key_exists($field, $filterableFields)) {
                $fieldType = $filterableFields[$field];

                switch ($fieldType) {
                    case 'boolean':
                        $query->where($field, $value);
                        break;
                    case 'text':
                        if (isset($value['contains'])) {
                            $query->where($field, 'like', '%' . $value['contains'] . '%');
                        }
                        break;
                    case 'date':
                        if (isset($value['from'])) {
                            $query->whereDate($field, '>=', $value['from']);
                        }
                        if (isset($value['to'])) {
                            $query->whereDate($field, '<=', $value['to']);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        foreach ($sort as $sortParam) {
            $query->orderBy($sortParam['field'], $sortParam['value']);
        }
        $query->skip($startRow)->take($endRow - $startRow + 1);
        $todos = $query->get();

        return TodoResource::collection($todos);
    }

    public function show($id)
    {
        $todo = TodoList::with('user')->findOrFail($id);

        return new TodoResource($todo);
    }

    public function store(TodoRequest $request)
    {
        if (TodoList::insert($request->validated())) {
            return response()->json(['message' => 'Todo stored successfully']);
        }
        return response()->json(['message' => 'Failed to store Todo', 500]);
    }

    public function update(TodoRequest $request, $todo)
    {
        if (TodoList::where('id', $todo)->update($request->validated())) {
            return response()->json(['message' => 'Todo update successfully']);
        }
        return response()->json(['message' => 'Failed to update Todo', 500]);
    }

    public function destroy(TodoList $todo)
    {
        if ($todo->delete()) {
            return response()->json(['message' => 'Todo deleted successfully']);
        }
        return response()->json(['message' => 'Failed to deleted Todo', 500]);
    }
}
