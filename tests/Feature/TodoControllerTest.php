<?php

namespace Tests\Feature;

use App\Models\TodoList;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TodoControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_can_get_all_todos()
    {
        TodoList::factory()->count(5)->create();

        $response = $this->get('api/todos');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'title',
                    'description',
                    'user',
                ],
            ],
        ]);
        $response->assertJsonCount(5, 'data');
    }

    public function test_can_get_single_todo()
    {
        $todo = TodoList::factory()->create();

        $response = $this->get('api/todos/' . $todo->id);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $todo->id,
            ],
        ]);
    }

    public function test_can_create_todo()
    {
        $user = User::factory()->create();

        $data = [
            'title' => 'Test Todo',
            'description' => 'This is a test todo',
            'user_id' => $user->id,
        ];

        $response = $this->post('api/todos', $data);

        $response->assertStatus(200);

        $response->assertJson([
            'message' => 'Todo stored successfully',
        ]);
    }

    public function test_can_update_todo()
    {
        $todo = TodoList::factory()->create();

        $data = [
            'title' => 'Updated Todo',
            'description' => 'This is an updated todo',
        ];
        $response = $this->put('api/todos/' . $todo->id, $data);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => $data,
        ]);
        $this->assertDatabaseHas('todo_lists', $data);
    }

    public function test_can_delete_todo()
    {
        $todo = TodoList::factory()->create();

        $response = $this->delete('api/todos/' . $todo->id);

        $response->assertStatus(200);
        $this->assertDatabaseMissing('todo_lists', ['id' => $todo->id]);
    }
}
