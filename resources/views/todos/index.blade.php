@extends('layouts.app')
@section('content')
    <h1>Todo list</h1>
    <style>
        .ag-root-wrapper-body {
            height: 100% !important;
        }
    </style>
    <div id="myGrid" style="width:100%; height: 100%" class="ag-theme-alpine"></div>
    <script>
        let agTextColumnFilter = {
            filterOptions: ['contains'],
            textFormatter: (r) => {
                if (r == null) return null;

                return r
                    .toLowerCase()
                    .replace(/[àáâãäå]/g, 'a')
                    .replace(/æ/g, 'ae')
                    .replace(/ç/g, 'c')
                    .replace(/[èéêë]/g, 'e')
                    .replace(/[ìíîï]/g, 'i')
                    .replace(/ñ/g, 'n')
                    .replace(/[òóôõö]/g, 'o')
                    .replace(/œ/g, 'oe')
                    .replace(/[ùúûü]/g, 'u')
                    .replace(/[ýÿ]/g, 'y');
            },
            maxNumConditions: 1,
        };
        let agDateColumnFilter = {
            filterOptions: ['inRange'],
            maxNumConditions: 1
        };
        const gridOptions = {
            pagination: true,
            paginationPageSize: 10,
            cacheBlockSize: 10,
            columnDefs: [
                {
                    field: 'title',
                    filter: 'agTextColumnFilter',
                    filterParams: agTextColumnFilter,
                    cellRenderer: function (params) {
                        return '<a href="{{'edit/'}}' + params.data.id + '">' + params.value + '</a>';
                    },
                },
                {
                    field: 'user.name',
                    sortable: false,
                    filter: false
                },
                {
                    field: 'description', filter: 'agTextColumnFilter',
                    filterParams: agTextColumnFilter,
                },
                {
                    field: 'completed', filter: 'agSetColumnFilter', filterParams: {
                        values: ['yes', 'no']
                    }
                },
                {field: 'date', filter: 'agDateColumnFilter', filterParams: agDateColumnFilter},
                {field: 'created_at', filter: 'agDateColumnFilter', filterParams: agDateColumnFilter},
                {field: 'updated_at', filter: 'agDateColumnFilter', filterParams: agDateColumnFilter},
            ],

            defaultColDef: {
                sortable: true,
                flex: 1,
                minWidth: 100,
                filter: true,
                rowData: null,
            },
            rowModelType: 'serverSide',
        };

        document.addEventListener('DOMContentLoaded', function () {
            let gridDiv = document.querySelector('#myGrid');
            new agGrid.Grid(gridDiv, gridOptions);

            fetch('https://www.ag-grid.com/example-assets/olympic-winners.json')
                .then((response) => response.json())
                .then(function (data) {
                    const datasource = createServerSideDatasource();
                    gridOptions.api.setServerSideDatasource(datasource);
                });
        });

        function implodeArray(array, delimiter) {
            return array.map(obj => Object.entries(obj).map(([key, value]) => `${key}=${value}`)).join(delimiter);
        }

        function createServerSideDatasource() {
            return {
                getRows: (params) => {
                    let sortModel = params.request.sortModel
                    let queryParams = [];
                    if (sortModel) {
                        sortModel.forEach(function (sortParam, index) {
                            let colId = sortParam.colId;
                            let sort = sortParam.sort;
                            queryParams.push(`sort[${index}][value]=${sort}&sort[${index}][field]=${colId}`);
                        });
                    }

                    if (params.request.endRow !== undefined) {
                        queryParams.push('endRow=' + params.request.endRow)
                    }
                    if (params.request.startRow !== undefined) {
                        queryParams.push('startRow=' + params.request.startRow)
                    }

                    let filterModel = params.request.filterModel;
                    if (filterModel) {
                        Object.keys(filterModel).forEach(function (key) {
                            let filter = filterModel[key];

                            if (filter.filterType === "set") {
                                let values = filter.values;
                                if (values && values.length > 0) {
                                    let valuesParam = values.map(value => encodeURIComponent(value)).join(',');
                                    queryParams.push(`filter[${key}]=${valuesParam}`);
                                }
                            } else if (filter.filterType === "text") {
                                let type = filter.type;
                                let filterValue = encodeURIComponent(filter.filter);
                                queryParams.push(`filter[${key}][${type}]=${filterValue}`);
                            } else if (filter.filterType === "date") {
                                let dateFrom = encodeURIComponent(filter.dateFrom);
                                let dateTo = encodeURIComponent(filter.dateTo);
                                queryParams.push(`filter[${key}][from]=${dateFrom}&filter[${key}][to]=${dateTo}`);
                            }
                        });
                    }
                    queryString = queryParams.join('&');
                    fetch('{{url('api/todos')}}?' + queryString)
                        .then(response => response.json())
                        .then(data => {
                            params.success({rowData: data.data})
                        })
                        .catch(error => {
                            console.error('Error:', error);
                        });
                },
            };
        }
    </script>
@endsection
