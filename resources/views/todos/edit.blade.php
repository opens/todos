@extends('layouts.app')

@section('content')
    <script>
        let todoId = {{ $id }}
    </script>

    <h1>Edit Todo</h1>
    <form id="editTodoForm">
        <div class="mb-3">
            <label for="title" class="form-label">Title:</label>
            <input type="text" id="title" name="title" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description:</label>
            <textarea id="description" name="description" class="form-control" required></textarea>
        </div>
        <div class="mb-3">
            <label for="date" class="form-label">Date:</label>
            <input type="text" id="date" name="date" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="completed" class="form-check-label">Completed:</label>
            <input type="checkbox" id="completed" name="completed" class="form-check-input">
        </div>
        <div class="mb-3">
            <label for="user" class="form-label">User:</label>
            <select id="user" name="user_id" class="form-select" required data-live-search="true">
                <option value="">Select a user</option>
                @foreach($users as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                @endforeach
            </select>
        </div>
        <div class="mt-5">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>

    <script>
        $(document).ready(function () {
            $('#date').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true
            });
            $('#user').select2({
                theme: 'bootstrap',
                placeholder: 'Select a user',
                allowClear: true
            });

            // Fetch the existing todo data and populate the form
            $.ajax({
                url: '{{ url('api/todos') }}/' + todoId,
                method: 'GET',
                dataType: 'json',
                success: function (response) {
                    $('#title').val(response.data.title);
                    $('#description').val(response.data.description);
                    $('#date').val(response.data.date);
                    $('#completed').prop('checked', response.data.completed);
                    $('#user').val(response.data.user.id).trigger('change');
                },
                error: function (xhr, status, error) {
                    console.error(xhr.responseJSON);
                    alert('Failed to fetch Todo');
                }
            });

            $('#editTodoForm').on('submit', function (e) {
                e.preventDefault();

                var formData = {
                    title: $('#title').val(),
                    description: $('#description').val(),
                    completed: $('#completed').is(':checked') ? 1 : 0,
                    date: $('#date').val(),
                    user_id: $('#user').val(),
                    _token: '{{ csrf_token() }}',
                    _method: 'PUT'
                };

                $.ajax({
                    url: '{{ url('api/todos') }}/' + todoId,
                    method: 'PUT',
                    dataType: 'json',
                    data: formData,
                    success: function (response) {
                        alert('Todo updated successfully');
                    },
                    error: function (xhr, status, error) {
                        console.error(xhr.responseJSON);
                        alert('Failed to update Todo');
                    }
                });
            });
        });
    </script>
@endsection
