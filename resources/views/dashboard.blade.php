@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Dashboard</h1>
        <div class="row">
            <div class="col-md-4">
                <h3>This day</h3>
                <p>Task count: {{ $todayTasks }}</p>
                <canvas id="todayChart"></canvas>
            </div>
            <div class="col-md-4">
                <h3>This week</h3>
                <p>Task count: {{ $weekTasks }}</p>
                <canvas id="weekChart"></canvas>
            </div>
            <div class="col-md-4">
                <h3>This month</h3>
                <p>Task count: {{ $monthTasks }}</p>
                <canvas id="monthChart"></canvas>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Task schedule by day</h1>
                <canvas id="todoChart"></canvas>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            let tasksData = <?php echo json_encode($tasksData); ?>;

            let labels = tasksData.map(function (task) {
                return task.date;
            });

            let taskCounts = tasksData.map(function (task) {
                return task.task_count;
            });

            let minDate = labels.reduce(function (a, b) {
                return a < b ? a : b;
            });

            let maxDate = labels.reduce(function (a, b) {
                return a > b ? a : b;
            });

            let ctx = document.getElementById('todoChart').getContext('2d');
            let chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        label: 'Number of tasks',
                        data: taskCounts,
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true,
                            stepSize: 1
                        },
                        x: {
                            ticks: {
                                min: minDate,
                                max: maxDate
                            }
                        }
                    }
                }
            });
        });
    </script>
@endsection
